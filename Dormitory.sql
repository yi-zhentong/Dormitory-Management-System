use dormitory;

-- 展示所有的数据库
show databases;

-- 创建数据库
create database if not exists dormitory default charset UTF8MB4;

-- 创建此数据库的专属用户
create user 'dormitoryManager'@'localhost' identified by '123456';

-- 查询用户
select * from mysql.user;

-- 授权
grant all on dormitory.* to 'dormitoryManager'@'localhost';

-- 建表

-- 用户表
-- 公司开发的删除为逻辑删除，即通过一个字段的值，判断这个数据是否已经删除
create table if not exists tb_user(
    id int primary key auto_increment comment '用户ID',
    username varchar(20) not null comment '用户名',
    password varchar(20) not null comment '密码',
    stu_code varchar(20) default null unique comment '学号',
    dorm_Code varchar(20) default null comment '宿舍编号',
    sex varchar(5) default null comment '性别',
    tel varchar(15) default null comment '手机号',
    dormBuildId int default null comment '宿舍楼ID',
    role_id tinyint default null comment '0-超级管理员，1-宿管，2-学生',
    create_user_id int default null comment '创建人的id',
    disabled tinyint default 0 comment '是否删除，0-不删，1-删'
)comment '用户表，包括了超级管理员、宿管和学生';
-- 添加外键
alter table tb_user add constraint tb_user_ibfk_1 foreign key (dormBuildId) references tb_dormbuild (id);
alter table tb_user add constraint tb_user_ibfk_2 foreign key (create_user_id) references tb_user (id);


-- 宿舍楼表
-- 公司开发的删除为逻辑删除，即通过一个字段的值，判断这个数据是否已经删除
create table if not exists tb_dormbuild(
    id int primary key auto_increment comment '宿舍楼的ID',
    name varchar(20) default null unique comment '宿舍楼的名称',
    remark varchar(50) default null comment '备注',
    disabled tinyint default 0 comment '是否删除，0-不删，1-删'
)comment '宿舍楼表';
