<%--
  Created by IntelliJ IDEA.
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>

<html>
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>404,找不到页面</title>

    <!--内嵌式的CSS样式设计-->
    <style>
        .head404{ width:580px; height:234px; margin:50px auto 0 auto;}

        .txtbg404{ width:499px; height:169px; margin:10px auto 0 auto;}

        .txtbg404 .txtbox{ width:390px; position:relative; top:30px; left:60px;color:#eee; font-size:14px;}

        .txtbg404 .txtbox p {margin:5px 0; line-height:18px;}

        .txtbg404 .txtbox .paddingbox { padding-top:15px;}

        .txtbg404 .txtbox p a { color:#eee; text-decoration:none;}

        .txtbg404 .txtbox p a:hover { color:#FC9D1D; text-decoration:underline;}
    </style>
</head>

<%--网页的结构主体--%>
<body bgcolor="#494949">
<div class="head404"></div>

    <div class="txtbg404">

        <div class="txtbox">

            <p>对不起，您请求的页面不存在、或已被删除、或暂时不可用</p>

            <p class="paddingbox">请点击以下链接继续浏览网页</p>

            <p>》<a style="cursor:pointer" onclick="history.back()">返回上一页面</a></p>

            <p>》<a href="./Logout">返回网站首页</a></p>

        </div>

    </div>
</body>
</html>
