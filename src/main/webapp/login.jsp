<%--
  Created by IntelliJ IDEA.
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<html lang="zh">

<%--宿舍管理系统登录的主界面--%>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>宿舍管理系统登录</title>

    <!--外联式的CSS样式设计-->
    <link href="./bootstrap/css/bootstrap.css" rel="stylesheet">

    <!--外联式的JS-->
    <script type="text/javascript" src="./bootstrap/js/jQuery.js"></script>

    <!--内嵌式的JS-->
    <script type="text/javascript">
        function checkForm() {
            //判断用户是否输入的学号和密码
            var stuCode = document.getElementById("stuCode").value;
            var password = document.getElementById("password").value;
            if (stuCode == null || stuCode === "") {
                document.getElementById("error").innerHTML = "学号不能为空";
                return false;
            }
            if (password == null || password === "") {
                document.getElementById("error").innerHTML = "密码不能为空";
                return false;
            }

            return true; //返回true，才提交表单
        }

        $(document).ready(function(){
            $("#login").addClass("active");
        });

    </script>

    <!--内嵌式的CSS-->
    <style>
        body {
            padding-top: 200px;
            padding-bottom: 40px;
            background-image: url('./images/1.jpg');
            background-position: center;
            background-repeat: no-repeat;
            background-attachment: fixed;
        }

        .radio {
            padding-top: 10px;
            padding-bottom:10px;
        }

        .form-signin-heading{
            text-align: center;
        }

        .form-signin {
            max-width: 300px;
            padding: 19px 29px 0px;
            margin: 0 auto 20px;
            background-color: #fff;
            border: 1px solid #e5e5e5;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
            -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
            -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
            box-shadow: 0 1px 2px rgba(0,0,0,.05);
        }
        .form-signin .form-signin-heading,
        .form-signin .checkbox {
            margin-bottom: 10px;
        }
        .form-signin input[type="text"],
        .form-signin input[type="password"] {
            font-size: 16px;
            height: auto;
            margin-bottom: 15px;
            padding: 7px 9px;
        }
    </style>

</head>

<%--网页的结构主体--%>
<body>
    <!--表单-->
    <form name="myForm" class="form-signin" action="Login" method="post" onsubmit="return checkForm()">
        <h2 class="form-signin-heading"><font color="gray">宿舍管理系统</font></h2>
        <input type="hidden"  name="action"  value="submit">

        <!--账号密码输入栏-->
        <!-- 如果想让控件像块级元素一样占满容器，就可以为它添加 .input-block-level 类。这样，控件不仅可以占满容器，还可以根据浏览器窗口自动调整尺寸 -->
        <input id="stuCode" name="stuCode" value="${stuCode}" type="text" class="input-block-level" placeholder="学号...">
        <input id="password" name="password" value="${password}" type="password" class="input-block-level" placeholder="密码..." >

        <!--角色输入栏-->
        <div id="role">
            <label style="display: inline-block">
                <input type="radio" name="roleId" id="role1" value="0" ${role eq 0 ? "checked" : ""} style="height:10px; vertical-align:top;"><span>超级管理员</span>
            </label>

            <label style="display: inline-block">
                <input type="radio" name="roleId" value="1" ${role eq 1 ? "checked" : ""} style="height:10px; vertical-align:top;"><span>宿舍管理员</span>
            </label>

            <label style="display: inline-block">
                <input type="radio" name="roleId" value="2" ${role eq 2 ? "checked" : ""} style="height:10px; vertical-align:top;"><span>学生</span>
            </label>
        </div>


        <!--记住我和出错时的提示语-->
        <label class="checkbox" style="margin-top: 10px">
            <input id="remember" name="remember" type="checkbox" value="remember-me" >记住我 &nbsp;&nbsp;&nbsp;&nbsp;
            <font id="error" color="red">${error}</font>
        </label>

        <!--下方的两个按钮-->
        <div style="text-align: center; margin-bottom: 20px">
            <!--按钮类型为submit：点击按钮，会将表单的内容提交到 action属性值login的请求处理类中 -->
            &nbsp;&nbsp; <button class="btn btn-large btn-primary" type="submit">登录</button> &nbsp;&nbsp;&nbsp; <button class="btn btn-large btn-primary" type="button" id="resetBtn">重置</button>
            &nbsp;&nbsp;&nbsp;&nbsp;
        </div>

        <script type="text/javascript">
            //重置按钮
            document.querySelector("#resetBtn").addEventListener("click", function ()
            {
                document.querySelector("#stuCode").value = '';
                document.querySelector("#password").value = '';
                document.querySelector("#role1").checked = true;
            });
        </script>

    </form>
</body>
</html>