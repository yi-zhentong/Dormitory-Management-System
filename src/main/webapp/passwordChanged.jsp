<%--
  Created by IntelliJ IDEA.
  User: 易振通
  Date: 2022/12/22
  Time: 16:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>

<%--修改密码的页面--%>
<script type="text/javascript">

    //校验函数
    function checkTable()
    {
        var realOldPassword = "${currentUser.password}"; //真正的原密码
        var oldPassword = document.getElementById("oldPassword").value; //原密码
        var newPassword = document.getElementById("newPassword").value; //新密码
        var rPassword = document.getElementById("rPassword").value; //重复的新密码

        $("#error").html(""); //初始化错误信息
        if(oldPassword === "" || newPassword === "" || rPassword === "")
        {
            document.getElementById("error").innerHTML = "信息填写不完整！";
            return false;
        } else if(newPassword === oldPassword){
            document.getElementById("error").innerHTML = "修改前后密码不能一致！";
            return false;
        } else if(newPassword !== rPassword){
            document.getElementById("error").innerHTML = "新密码两次填写不一致！";
            return false;
        } else if (realOldPassword !== oldPassword)
        {
            document.getElementById("error").innerHTML = "原密码输入错误!";
            return false;
        }

        return true;
    }

    //选中
    $(document).ready(function(){
        $("#password").addClass("active");
    });
</script>

<div class="data_list">

    <div class="data_list_title">
        修改密码
    </div>

    <form action="passwordAction?action=change" method="post" onsubmit="return checkTable()">
        <div class="data_form" >
            <table align="center">
                <tr>
                    <td><font color="red">*</font>原密码：</td>
                    <td><input type="password"  id="oldPassword"  name="oldPassword" style="margin-top:5px;height:30px;" /></td>
                </tr>

                <tr>
                    <td><font color="red">*</font>新密码：</td>
                    <td><input type="password" id="newPassword"  name="newPassword" value="" style="margin-top:5px;height:30px;" /></td>
                </tr>

                <tr>
                    <td><font color="red">*</font>重复密码：</td>
                    <td><input type="password" id="rPassword"  name="rPassword" value="" style="margin-top:5px;height:30px;" /></td>
                </tr>

            </table>

            <div align="center">
                <input type="submit" class="btn btn-primary" value="提交"/>
            </div>

            <div align="center">
                <input type="hidden" id="id"  name="id" value="${currentUser.id}" />
                <font id="error" color="red"></font>
            </div>

        </div>
    </form>
</div>
