<%--
  Created by IntelliJ IDEA.
  User: 易振通
  Date: 2022/12/22
  Time: 21:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>


<script type="text/javascript">

    $(document).ready(function(){
        $("#info").addClass("active");
    });

</script>

<%--查看学生的信息--%>
<div class="data_list">

    <!--标题-->
    <div class="data_list_title">
        查看信息
    </div>

    <div class="data_form" >

        <table align="center" border="1px">
            <tr style="text-align: center; margin: 10px">
                <td>工号：</td>
                <td>${admin.stu_code}</td>
            </tr>

            <tr style="text-align: center; margin: 10px">
                <td>姓名：</td>
                <td>${admin.username}</td>
            </tr>

            <tr style="text-align: center; margin: 10px">
                <td>性别：</td>
                <td>${admin.sex}</td>
            </tr>

            <tr style="text-align: center; margin: 10px">
                <td>手机号：</td>
                <td>${admin.tel}</td>
            </tr>

        </table>
    </div>
</div>